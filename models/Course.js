/*
The naming convention for model files is the singular and capitalized form of the name of the document.
*/

const mongoose = require("mongoose");

/*
	Mongoose Schema

	Before we can create documents from our API to save into outr database, we first datermine the structure of the document to be written in the database.

	Schema acts as a blueprint for our data/document

	a schema is a representation of how the document is structured. it also determine the type of data the expected properties.

	So, with this, we won't have to worry for if we had input "stock" or "stock" in our documents, because can majke it uniform with a schema.

	In mongoose, to create a schema, we use the Schema() constructor from mongoose. This will allow us to create a mongoose schema object.
*/


	const courseSchema = new mongoose.Schema({

		name: {
			type:String,
			required: [true,"Name is required"]
		},
		description: {
			type:String,
			required: [true,"Description is required"]
		},
		price: {
			type: Number,
			required: [true,"Price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type:String,
					required: [true,"User Id is required"]
				},
				dateEnrolled: {
					type:Date,
					default: new Date()
				},
				status:{
					type:String,
					default: "Enrolled"
				}
			}
		]

	})


	// module.exports -so we can import and use thie file in another file.

	/*
		Mongoose Model

		Is the connection to our collection.

		It has two arguments, first the name of the collection the model is going to connect to. In mongDB, once we create a new course document this model will connect to our collection. But since the course collection is not yet created initially, mongoDB will create it for us.

		The Second argument is the schema of the document in the collection.

	*/
	module.exports = mongoose.model("Course",courseSchema);