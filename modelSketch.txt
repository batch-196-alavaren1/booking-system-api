App: Booking System API
Description: Allow a iser to enroll to a course. Allows an admin to do CRUD operations. Allow users to register into database. 

course
id,
name - string,
description - string,
price - number,
isActive - boollean,
		   default: true
enrollees: [
	{
		id,
		userid,
		status - string,
		dateEnrolled - date

	}
]


user
id,
firstName - string,
lastName - string,
email -string,
password -string,
mobileNo - string,
isAdmin - boolean,
		  default: false 
enrollment: [
	{
		id,
		courseid,
		status - string,
		dateEnrolled - date
	}

]


In mongoDB, we can translate uor associative entity as an embedded subdocument array with the use of Two Way Embedding.

enrollment
id,
courseid,
userid,
status - string,
dateEnrolled - date
