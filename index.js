const express = require("express");
const app = express();
/*Mongoose is an ODM library lo let our */
const mongoose = require("mongoose");
app.use(express.json());
const port = 4000;

/*
Mongoose Connection 

	mongoose.connect() is a method to connect out api with our mongdb database vai the use of mongoose. It has 2 arguments. first, is the connection string to connect our api to our mongdb. Second, is an object
*/
	


mongoose.connect("mongodb+srv://admin:admin123@cluster0.bowgdmr.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true
});

// we will create notification if the conneciton to the db is a sucess or failed.
let db = mongoose.connection
// This is to show notification of an internal server error from MongoDB
db.on('error',console.error.bind(console, "MongoDB Connection Error."));
// If the connection is open and successful, we will outpu a message in the terminal/gitbash:
db.once('open',()=>console.log("Connected to MongoDB"));



// express.json() to able to handle the request body and parse it into JS Object
app.use(express.json());



// import our routes and use it as middleware.
// Which means, that we will be able to groip together out routes
const courseRoutes = require('./routes/courseRoutes');
// console.log(courseRoutes);


// Use our routes and group them together under 'courses'
// our endpoint are now prefaced with endpoint
app.use('/courses',courseRoutes);


const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);



app.listen(port,() => console.log(`Express API running at port 4000`))