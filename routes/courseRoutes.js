/*
	To be able create routes from another file to be used in our application, from here, we have to import express as well, however, we wil now use anthoer method form express to contain our routes.

	The Router() Method, will use to contain our routes

*/

const express = require("express");
const router = express.Router();



const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

const { verify,verifyAdmin } = auth;

// All routes to course now has an endpoint prefaced with/courses

// endpoint - /courses/
// Gets all course documents whether it is active or inactive
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

// endpoint - /courses/
// Only logged in user is able to use addCourse
// verifyAdmin() will disallow regular logged in and non-logged in users from using route
router.post('/',verify,verifyAdmin,courseControllers.addCourse,);


// Get all active courses - (regular, non-looged in)
router.get('/activeCourses',courseControllers.getActiveCourses);

/*
	Activity s36-s37
*/

// get single course
// we can pass data in a route without the use of request body by passing a small amount of data through the url with use of route params
// /endpoint/:routeParams
// http://localhost:4000/courses/getSingleCourse/62e75249cffa9f2391591de6
router.get('/getSingleCourse/:courseId',courseControllers.getSingleCourse);

// update a single course
// pass the id of the course we want to update via route params
// the update details will be passed via request body
router.put("/updateCourse/:courseId",verify,verifyAdmin,courseControllers.updateCourse);

// archive a single course
// Pass the id for the course we want to update via route params
// We will directly update the course as inactive
router.delete("/archiveCourse/:courseId",verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;