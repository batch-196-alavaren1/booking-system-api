const express = require("express");
const router = express.Router();





// In fact, routes should just contain the endpoints and it should only trigger the function but it should not be where define the logic of our functions.

//  the business logic of our API should be in controllers.
// import our user controllers:
const userControllers = require("../controllers/userControllers");


// check the import userControllers:
// console.log(userControllers);



// import auth to ba able to have access and use the verify methods to act as middleware for our routes.
// Middleware add in the routes such as verify() will have access to the req,res,object

const auth = require("../auth");
// destructure auth to get only our methods and save it in variables;
const { verify } = auth;

/*
	Updated Routes Syntax:

	router.method("/endpoint",handerFunction)

*/

// Register
router.post("/",userControllers.registerUser);


/*Post method route to get user details by id:*/
// http://localhost:4000/users/details 
// Verify() is used as a middleware wich means our request will get through verify first before our controller
// Verify() will not only check the valitity of the token but also add the decoded data of the token in the request object as req.user

router.get('/details',verify,userControllers.getUserDetails);



// Route for use Authentication
router.post('/login',userControllers.loginUser);


/*
	Activity s36-s37
*/


router.post('/checkEmail',userControllers.checkEmail)


// User Enrollment
// courseId will come from the req.body
// userId will come from the req.user
router.post('/enroll',verify,userControllers.enroll);



module.exports = router;


