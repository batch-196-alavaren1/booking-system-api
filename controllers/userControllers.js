/*

Naming convention for controllers is that it should be named after the model document it is concerned with

Controller are functions which contain the actual business logic of our API and is triggered by a route.

*/

// To create a controller, we first add it into our module.exports.
//  So that we can import the controllers from our module.

// Import the user model in the controllers instead because this is where we are now going to use it.

const User = require("../models/User");

// import course
const Course = require("../models/Course")

// bcrypt is a packcage allow us to hash out password to add layer of security for our user's details
// import bcrypt 
const bcrypt = require("bcrypt");

// import auth.js module to use createAccessToke and its subsequent methods
const auth = require("../auth");


module.exports.registerUser = (req,res)=>{

	// check the onput passed via the client
	// console.log(req.body) 

	// Using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds is the number of times we randomized the string/password hash
	// bcrypt.hashSync<string>,<saltrounds>
	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	});

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


// getUserDetails should only allow the LOOGED - in user his OWN details
module.exports.getUserDetails = (req,res)=>{
	// console.log(req.body);

	// console.log(req.user);// contacils of the looged in user


	// findOne() will return a single document that matched our rcriteria.
	// User.find({_id:req.body._id})
	// User.findOne({_id:req.body.id})

	// This will allow us to secure that the LOGGED IN USER or the USER THAT PASSED THE TOKEN will be able to get HIS OWN details and ONLY his own.
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res) => {


	console.log(req.body);

	/*
		Step for logging in our user:

		1. find the user by its email
		2. If we found the user, we will check his password if the password input and the hashed the password in our db matches.
		3. If we don't find a user, we will send a message to the client.
		4. If upon checking the found usr's password is the same our input password, we will generate a "key" for the use to have authorization to access certain feature in our app.

	*/

	// mongodb: db.user.findOne({email:req.body.email})
	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contain the result if fondOne
		//find() returns null 
		if(foundUser === null){
			return res.send({message:"No User Found."})//Client will receive this object with out message if no user is found
		} else {
			// console.log(foundUser)
			// Check if the password from req.body matches the hashed password in our foundOne document.
			/*
				bcrypt.compareSync(<inputString>,<hashedString>)
				

				if the inputString and the hashedString matches and are the same, the compareSync method will return true.
				else, it will return false.

			*/

			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)


			// console.log(isPasswordCorrect);

			// If the password if correct, we will create a "key", a token for our user, else, we will send a message:
			if(isPasswordCorrect){

				/*
					To be able to create a "key" or token that allows/authorizes
					our logged in user around our application, we have to create our own module called auth.js.

					this module create a encoded string which contains our user's details.

					this encoded string is what we call JSONwebToken or JWT.


				*/
				// console.log("We will create a token for the user if the password is correct")

					// auth.createAccessToken receives our founfUser document as an argument, get only necessary details, wrap those details. wrap those details in JWT and our secret, then finally return our JWT. This JWT will be sent to our client.
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else{

				return res.send({message: "Incorrect Password"});

			}
		}
	})
}

	/*
		Activity s36-s37
	*/
// check if email exists or not
	module.exports.checkEmail = (req,res) => {

	User.findOne({email:req.body.email})
	.then(result => {

		// findOne will return null if no match is found
		// send false if email does not exist
		// send true if email exists
		if(result === null){
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(error => res.send(error))
	}


module.exports.enroll = async (req,res) => {

	// check the id of the user who will enroll?
	// console.log(req.user.id);

	// check the id of the course we want to enroll?
	// console.log(req.body.courseId);

	// Validate the user if they are an admin or not.
	// If the user is an admin, send a message to cliend and end the response
	// Else, we will continue
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden."});
	} 
	/*
		Enrollment will come in two steps

		First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in th enrollments array.
		Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.


		Since we will aceess 2 collections in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line.


		async and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behavior of running each code line by line we will be able to wait for the result of a function.

		To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding.


	*/

	// return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollement subdocumet array.
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		// check if you found the user's document:
		// console.log(user);

		// Add the courseId in an object and push that object into the user's enrollment
		// Because we have to follow the schema of the enrollments subdocument array

		let newEnrollment = {

			courseId: req.body.courseId
		}

		// access the enrollment array from our user an push the new enrollment subdocument into the enrollments array
		user.enrollments.push(newEnrollment)


		// We must save the user document and return the value of saving our document
		// return true IF we push the subdocument successfully
		//catch and return error message otherwise
		return user.save().then(user => true).catch(err => err.message)

	})
	// If user was able to enroll properly, isUserUpdated contains true
	// Else, isUserUpdated will contain an error message
	// console.log(isUserUpdated);

	// Add an if statement and stops the process IF isUserUpdated not contain true
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	// Find the course where we will enroll or add the user as an enrollee and return true IF we were able to push the user into the enrollees array properly or send the error message instead.
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		// console.log(course);//contain the found course we want to enroll in.


		// create an object to pushed into the subdocument array, enrollees
		// we have to follow the schema of our subdocument array
		let enrollee = {
			userId: req.user.id
		}

		// push the enrollee into the enrollees subdocument array of the course:
		course.enrollees.push(enrollee);

		// save the course document 
		// return true IF we were able to save and add the user as enrollee properly
		// return an err message if we catch an error
		return course.save().then(course => true).catch(err => err.message);
	})
	// console.log(isCourseUpdated);

	// if isCourseUpdated does not contain true, send the error message to the client and stop the process
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	// ensure that we were able to both update the user and course document to add our enrollment and enrollee respectively and send a message to the client to end the enrollment process
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling!"})
	}

}